# Comandos úteis
```
ssh-keygen -t rsa -f ~/.ssh/id_k8s_lab -C k8s_node
```

```
mkdir ansible

cd ansible

mkdir -p roles/{master,slave}/{tasks,handlers,templates,files}

touch roles/{master,slave}/{tasks,handlers,templates,files}/.gitkeep

touch master.yml slave.yml
```

# Referências

* [Boas práticas ansible](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html)